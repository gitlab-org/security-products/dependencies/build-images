# Build Images

Miscellaneous images used to build analyzer projects. This is very similar to [gitlab-build-images](https://gitlab.com/gitlab-org/gitlab-build-images), but with a much simpler and less flexible configuration.

## Adding your image

1. [Edit `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/security-products/dependencies/build-images/-/edit/main/.gitlab-ci.yml).
1. Add your `<image>` to the `IMAGE_NAME` list under the `.image-matrix` job template.
1. Create the corresponding `Dockerfile.<image>` file.
1. Push your changes to a new branch and, once everything looks good, create a merge request.

## Using images

Browse the [available images](https://gitlab.com/gitlab-org/security-products/dependencies/build-images/container_registry) and copy the address to use for your project.

If you want to be notified about changes to the images, [edit `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/security-products/dependencies/build-images/-/edit/main/.gitlab-ci.yml) and add a comment with a link to your project under the appropriate image name(s).
